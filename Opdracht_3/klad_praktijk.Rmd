---
title: "Klad_opdracht_3"
author: "Micha"
date: "13-5-2020"
output:
  html_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
head(data <- read.csv("MPL.csv", na.strings = "NA"))
medians <- aggregate(data[,c("MPL_conc","mRNA","Free_receptor")],list(data$dose,data$time), median, na.rm=T)
names(medians)[1:2] <- c("dose","time")
head(medians)
```

# Alternate models
## Base model
```{r}
# Create a function to simulate the Glucocorticoid receptor dynamica
GRD <- function(t, y, parms){
  with(as.list(c(y, parms)),{
    dmRNAr <- ks_Rm*(1-(DRN/(IC50_rm+DRN)))-kd_Rm*mRNAr
    dR <- ks_r*mRNAr+Rf*kre*DRN-kon*D*R-kd_R*R
    dDR <- kon*D*R-kT*DR
    dDRN <- kT*DR-kre*DRN
    return(list(c(dmRNAr, dR, dDR, dDRN)))
  })
}
```




```{r}
library(deSolve)

# Setting the parameters for the model
parameters <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)

# Set the start values for the simulation
startValues <- c(mRNAr = 4.74, R = 267, DR = 0, DRN = 0)

# Set the time frame. In this case 2 days in hours
times <- seq(0, 150, by=0.1)

# Perform the simulation using the ode method from deSolve
out <- ode(times = times, y = startValues, parms = parameters, func = GRD, method = euler)
```


```{r}
# Plot the simulated values
plot(out[, c(1,2)], main="Fig 1: Conc. of mRNAr over time", xlab = "Time (Hours)", ylab = "Conc. mRNAr (fmol/g liver)", ylim = c(0,5))
lines(mRNA[dose == 0.1]~time[dose == 0.1], data = medians)

plot(out[, c(1,3)], main="Fig 2: Conc. of free receptors(R) over time", xlab = "Time (Hours)", ylab = "Conc. R (fmol/mg protein)", ylim = c(0, 300))
lines(Free_receptor[dose == 0.1]~time[dose == 0.1], data = medians)
```



Questions:
[1] What would be the time course concentration of the activated drug-receptor complex if there was no auto-regulation of glucocorticoid receptor, i.e. if there was not effect of drug on the synthesis of the receptor mRNA? What formula needs to be changed? Adjust the model, run the simulation and plot the results to find out.

To do this the formula for the mRNAr
When the drug has no effect on the synthesis of the reseptor mRNA, you can see that the concentration of mRNAr over tiem does not change by much. This causes the Concentration of free receptors to drop faster but not further. What all leads to a higher concentration of the DR complex over time both in and outside the nucleus.
```{r}
# Create a function to simulate the Glucocorticoid receptor dynamica
GRD_no_reg <- function(t, y, parms){
  with(as.list(c(y, parms)),{
    dmRNAr <- ks_Rm-kd_Rm*mRNAr
    dR <- ks_r*mRNAr+Rf*kre*DRN-kon*D*R-kd_R*R
    dDR <- kon*D*R-kT*DR
    dDRN <- kT*DR-kre*DRN
    return(list(c(dmRNAr, dR, dDR, dDRN)))
  })
}
```

```{r}
out_no_reg <- ode(times = times, y = startValues, parms = parameters, func = GRD_no_reg, method = euler)
```

```{r}
n <- 4
par(oma = c(2,0,1,0), mfrow = c(floor(sqrt(n)), ceiling(sqrt(n))), mar = c(4, 4, 3, 0))

# A model where the drug has no effect on the syntisise of receptor mRNA
plot(out, out_no_reg, main=c("Fig 1: Conc. of mRNAr over time", "Fig 2: Conc. of free receptors(R) over time", "Fig 3: Conc. of DR complex over time", "Fig 4: Conc. of nucl. DR complex over time"), xlab = "Time (Hours)", ylab=c("Conc. mRNAr (fmol/g liver)", "Conc. R (fmol/mg protein)", "Conc. DR (fmol/mg protein)", "Conc. DR(N) (fmol/mg protein)"), col = c("black", "red"))

par(fig = c(0, 1, 0, 1), oma = c(0, 0, 0, 0), mar = c(0, 0, 0, 0), new = TRUE)

plot(0, 0, type = 'l', bty = 'n', xaxt = 'n', yaxt = 'n')

#legend(-5, 10, col = c("black", "red", "blue", "green", "purple"), c("Default", "k_on/5", "k_on/2", "k_on*5", "k_on*2"), pch = 19)
legend('bottom',legend = c("Default", "No DR(N) regulation"), col = c("black", "red"), lwd = 5, xpd = TRUE, horiz = TRUE, cex = 1, seg.len=1, bty = 'n')

```

[2] What is the time course of receptor and mRNA concentrations when the drug treatment is stopped? So After the steady state is reached (at time t_steady), D should be set to zero and the simulation should continue from time t_steady till the new steady state is reached (t_steady_second). Run the simulations and plot the results from t = 0 till t_steady_second.

The first 46 hours are the same as de standard model, but after the treatment has stoped the concentration of mRNAr and the concentration of free receptors rockets up close to the starting value at the begining of the simulator. While the Concentrations of the DR complex both in and outside the nucleus drop down to 0 mol/mg protein. This all happens in a time frame of 100 hours with the free receptors as the last thing to balance out.

```{r}
# Create a function to simulate the Glucocorticoid receptor dynamica
GRD_second_steady <- function(t, y, parms){
  with(as.list(c(y, parms)),{
    precisie <- 2
    if (steady_state >= 1){
      D <- 0
    }
    dmRNAr <- ks_Rm*(1-(DRN/(IC50_rm+DRN)))-kd_Rm*mRNAr
    dR <- ks_r*mRNAr+Rf*kre*DRN-kon*D*R-kd_R*R
    dDR <- kon*D*R-kT*DR
    dDRN <- kT*DR-kre*DRN
    steady_state <- 0
    #print(c(round(dmRNAr, precisie), round(dR, precisie), round(dDR, precisie),round(dDRN, precisie)))
    if (round(dmRNAr, precisie) == 0 && round(dR, precisie) == 0 && round(dDR, precisie) == 0 && round(dDRN, precisie) == 0){
      steady_state <- 1
    }
    return(list(c(dmRNAr, dR, dDR, dDRN, steady_state)))
  })
}
```

```{r}
# Set the start values for the simulation
startValuesSteady <- c(mRNAr = 4.74, R = 267, DR = 0, DRN = 0, steady_state = 0)

# Set the time frame. In this case 2 days in hours
times_second_steady <- seq(0, 175, by=1)
```


```{r}
out_second_steady <- ode(times = times_second_steady, y = startValuesSteady, parms = parameters, func = GRD_second_steady, method = euler)
```


```{r}
# A model where the drug is removed when it reaches a steady state
plot(out_second_steady, main=c("Fig 1: Conc. of mRNAr over time", "Fig 2: Conc. of free receptors(R) over time", "Fig 3: Conc. of DR complex over time", "Fig 4: Conc. of nucl. DR complex over time", "Fig 5: Number of Steady states over time"), xlab = "Time (Hours)", ylab=c("Conc. mRNAr (fmol/g liver)", "Conc. R (fmol/mg protein)", "Conc. DR (fmol/mg protein)", "Conc. DR(N) (fmol/mg protein)", "Number of steady state"))
```

[3] Different corticosteroids show different association rates from receptors (kon) and different dissociation rates (in this model reflected by kre). Assuming the same concentrations of the drug, what is the effect of different values of kon and kre (consider 2 and 5 times increase and decrease of both parameters separately) on the receptor and mRNA dynamics? Adjust kon and kre as below and plot the results of the simulation for each change. Note: Simulations should be run for 4 new values of kon: 0.00329/5, 0.00329/2, 0.00329*2 and 0.00329*5. The results should be compared to the basic scenario when kon=0.00329 Separately, simulations should be run for 4 new values of kre: 0.57/5, 0.57/2, 0.57*2 and 0.57*5. The results should be compared to the basic scenario when kre= 0.57.

With the higher multiplecation of k_on you can see a faster decline in the receptor mRNA and the concentration of free reseptors. With the concentration stabelizing at a lower concentration aswel. At the same time the concentration for DR and DR in the nucleus grows faster and stabalizes at a higher concentration compaired to the base value. While with a higher division of k_on you can see a slower decline in the concentration of receptore mRNA and the concentration of the free receptors, and stabelizing at a higher conentration compaired to the base value. While the concentration of DR and DR in the nucleus grow slower and stabelizing at a lower concentration than the base value. You can also see that the diverence between the values that are devided compaired to the base are greater than the diverence between the values that are multiplicated compaired to the base values.

With the multiplecation of k_re you see that the effects are reversed for the concentration of mRNAr, the concentration of free receptors and the concentration of DR complex in the nucleus. While the higher k_re does not change the peakes at the start for the DR complex you can still see that the concentrain stabalizes at a higher point compaired to the base. This reversal of the effect is also seen in with te values where k_re is devided with the DR complex as a outlier.

The change in k_re has the most impect on the concentration of the mRNAr and the concentration of the DR complex in de nucleus over time. While de change in k_on has the most impect on the concentration of the free receptor and the concetnration of the DR complex over time.

```{r}
parameters_k_on_def5 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329/5, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
parameters_k_on_def2 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329/2, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
parameters_k_on_mult5 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329*5, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
parameters_k_on_mult2 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329*2, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)

parameters_k_re_def5 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57/5, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
parameters_k_re_def2 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57/2, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
parameters_k_re_mult5 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57*5, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
parameters_k_re_mult2 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57*2, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
```

```{r}
out_k_on_def5 <- ode(times = times, y = startValues, parms = parameters_k_on_def5, func = GRD, method = euler)
out_k_on_def2 <- ode(times = times, y = startValues, parms = parameters_k_on_def2, func = GRD, method = euler)
out_k_on_mult5 <- ode(times = times, y = startValues, parms = parameters_k_on_mult5, func = GRD, method = euler)
out_k_on_mult2 <- ode(times = times, y = startValues, parms = parameters_k_on_mult2, func = GRD, method = euler)

out_k_re_def5 <- ode(times = times, y = startValues, parms = parameters_k_re_def5, func = GRD, method = euler)
out_k_re_def2 <- ode(times = times, y = startValues, parms = parameters_k_re_def2, func = GRD, method = euler)
out_k_re_mult5 <- ode(times = times, y = startValues, parms = parameters_k_re_mult5, func = GRD, method = euler)
out_k_re_mult2 <- ode(times = times, y = startValues, parms = parameters_k_re_mult2, func = GRD, method = euler)
```

```{r}
n <- 4
par(oma = c(2,0,1,0), mfrow = c(floor(sqrt(n)), ceiling(sqrt(n))), mar = c(4, 4, 3, 0))

plot(out, out_k_on_def5, out_k_on_def2, out_k_on_mult5, out_k_on_mult2, main=c("Fig 1: Conc. of mRNAr over time", "Fig 2: Conc. of free receptors(R) over time", "Fig 3: Conc. of DR complex over time", "Fig 4: Conc. of nucl. DR complex over time"), xlab = "Time (Hours)", ylab=c("Conc. mRNAr (fmol/g liver)", "Conc. R (fmol/mg protein)", "Conc. DR (fmol/mg protein)", "Conc. DR(N) (fmol/mg protein)"), col = c("black", "red", "blue", "green", "purple"))

par(fig = c(0, 1, 0, 1), oma = c(0, 0, 0, 0), mar = c(0, 0, 0, 0), new = TRUE)

plot(0, 0, type = 'l', bty = 'n', xaxt = 'n', yaxt = 'n')

#legend(-5, 10, col = c("black", "red", "blue", "green", "purple"), c("Default", "k_on/5", "k_on/2", "k_on*5", "k_on*2"), pch = 19)
legend('bottom',legend = c("Default", "k_on/5", "k_on/2", "k_on*5", "k_on*2"), col = c("black", "red", "blue", "green", "purple"), lwd = 5, xpd = TRUE, horiz = TRUE, cex = 1, seg.len=1, bty = 'n')


par(oma = c(2,0,1,0), mfrow = c(floor(sqrt(n)), ceiling(sqrt(n))), mar =c(4, 4, 3, 0))

plot(out, out_k_re_def5, out_k_re_def2, out_k_re_mult5, out_k_re_mult2, main=c("Fig 1: Conc. of mRNAr over time", "Fig 2: Conc. of free receptors(R) over time", "Fig 3: Conc. of DR complex over time", "Fig 4: Conc. of nucl. DR complex over time"), xlab = "Time (Hours)", ylab=c("Conc. mRNAr (fmol/g liver)", "Conc. R (fmol/mg protein)", "Conc. DR (fmol/mg protein)", "Conc. DR(N) (fmol/mg protein)"), col = c("black", "red", "blue", "green", "purple"))

par(fig = c(0, 1, 0, 1), oma = c(0, 0, 0, 0), mar = c(0, 0, 0, 0), new = TRUE)

plot(0, 0, type = 'l', bty = 'n', xaxt = 'n', yaxt = 'n')

#legend(-5, 10, col = c("black", "red", "blue", "green", "purple"), c("Default", "k_on/5", "k_on/2", "k_on*5", "k_on*2"), pch = 19)
legend('bottom',legend = c("Default", "k_re/5", "k_re/2", "k_re*5", "k_re*2"), col = c("black", "red", "blue", "green", "purple"), lwd = 5, xpd = TRUE, horiz = TRUE, cex = 1, seg.len=1, bty = 'n')

```

[4] What would happen if the synthesis of the receptor was completely blocked? Which parameter needs to be put to zero? Adjust the parameter, run the simulations and plot the results.

To stop the synthisis of of the free receptor the parameter ks_r needs to be zero. This will have the effect that when the starting concentration of free receptor is gone the concentration of DR and DR in the nucleus slowly disappear to zero, with the mRNAr concentration going back to equalibrium as if there was no medicine.

```{r}
# Setting the parameters for the model
parameters_no_r_syth <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 0, D = 20*1000/374.491)

# Perform the simulation using the ode method from deSolve
out_no_r_syth <- ode(times = times, y = startValues, parms = parameters_no_r_syth, func = GRD, method = euler)
```

```{r}
plot(out_no_r_syth, main=c("Fig 1: Conc. of mRNAr over time", "Fig 2: Conc. of free receptors(R) over time", "Fig 3: Conc. of DR complex over time", "Fig 4: Conc. of nucl. DR complex over time"), xlab = "Time (Hours)", ylab=c("Conc. mRNAr (fmol/g liver)", "Conc. R (fmol/mg protein)", "Conc. DR (fmol/mg protein)", "Conc. DR(N) (fmol/mg protein)"))
```

[5] What is the dynamic of the system when the baseline rate of production of mRNA of the receptor is increased or decreased 2 or 5 fold (recalculate the rate of mRNA degradation so that the steady-state assumption at baseline (without the drug) is still valid, i.e. mRNA levels are constant when there is not drug)? Mind you: ks_Rm values should be changed, but we know that if without the drug the system is at steady-state then kd_Rm = ks_Rm/Rm0. Therefore if we change ks_Rm we need to change kd_Rm as well. Also after we recalculate the value of kd_Rm for the baseline conditions, the simulations should be run with drug present. Simulations should be run for 4 different scenarios:
ks_Rm = 2.9/5 and kd_Rm=2.9/5/4.74
ks_Rm = 2.9/2 and kd_Rm=2.9/2/4.74
ks_Rm = 2.9*2 and kd_Rm=2.9*2/4.74
ks_Rm = 2.9*5 and kd_Rm=2.9*5/4.74

The effects on the system are minor when the production rate of the receptor mRNA are increased or decreased. The most effect can be observed in the concentration of the mRNAr With the lower production rate taking longer to reach the lowest concentration and with a gratual stabalization. While the increase in the production rate reaches the lowest point faster with a faster rise towards the stabalization point.

```{r}
# Setting the parameters for the model
parameters_baseline <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
# Setting the parameters for the model
parameters_ks_rm_def5 <- c(ks_Rm =  2.9/5, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 2.9/5/4.74, ks_r = 3.22, D = 20*1000/374.491)
# Setting the parameters for the model
parameters_ks_rm_def2 <- c(ks_Rm = 2.9/2, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 2.9/2/4.74, ks_r = 3.22, D = 20*1000/374.491)
# Setting the parameters for the model
parameters_ks_rm_mulit5 <- c(ks_Rm = 2.9*2, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 2.9*2/4.74, ks_r = 3.22, D = 20*1000/374.491)
# Setting the parameters for the model
parameters_ks_rm_multi2 <- c(ks_Rm = 2.9*5, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 2.9*5/4.74, ks_r = 3.22, D = 20*1000/374.491)
```

```{r}
# Perform the simulation using the ode method from deSolve
out_ks_rm_def5 <- ode(times = times, y = startValues, parms = parameters_ks_rm_def5, func = GRD, method = euler)

# Perform the simulation using the ode method from deSolve
out_ks_rm_def2 <- ode(times = times, y = startValues, parms = parameters_ks_rm_def2, func = GRD, method = euler)

# Perform the simulation using the ode method from deSolve
out_ks_rm_mulit5 <- ode(times = times, y = startValues, parms = parameters_ks_rm_mulit5, func = GRD, method = euler)

# Perform the simulation using the ode method from deSolve
out_ks_rm_multi2 <- ode(times = times, y = startValues, parms = parameters_ks_rm_multi2, func = GRD, method = euler)
```

```{r}
n <- 4
par(oma = c(2,0,1,0), mfrow = c(floor(sqrt(n)), ceiling(sqrt(n))), mar = c(4, 4, 3, 0))

plot(out, out_ks_rm_def5, out_ks_rm_def2, out_ks_rm_mulit5, out_ks_rm_multi2, main=c("Fig 1: Conc. of mRNAr over time", "Fig 2: Conc. of free receptors(R) over time", "Fig 3: Conc. of DR complex over time", "Fig 4: Conc. of nucl. DR complex over time"), xlab = "Time (Hours)", ylab=c("Conc. mRNAr (fmol/g liver)", "Conc. R (fmol/mg protein)", "Conc. DR (fmol/mg protein)", "Conc. DR(N) (fmol/mg protein)"), col = c("black", "red", "blue", "green", "purple"))

par(fig = c(0, 1, 0, 1), oma = c(0, 0, 0, 0), mar = c(0, 0, 0, 0), new = TRUE)

plot(0, 0, type = 'l', bty = 'n', xaxt = 'n', yaxt = 'n')

#legend(-5, 10, col = c("black", "red", "blue", "green", "purple"), c("Default", "k_on/5", "k_on/2", "k_on*5", "k_on*2"), pch = 19)
legend('bottom',legend = c("Default", "ks_rm/5", "ks_rm/2", "ks_rm*5", "ks_rm*2"), col = c("black", "red", "blue", "green", "purple"), lwd = 5, xpd = TRUE, horiz = TRUE, cex = 1, seg.len=1, bty = 'n')

```

