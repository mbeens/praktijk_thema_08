---
title: 'Glucocorticoid receptor dynamica '
author: "Micha Snippe, Ted van Es"
date: "`r Sys.Date()`"
output:
  html_document:
    df_print: paged
    fig_caption: yes
  word_document: default
  pdf_document:
    fig_caption: yes
    number_sections: yes
linkcolor: blue
header-includes:
- \usepackage{longtable}
- \usepackage{hyperref}
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Introduction
For this assignment we are going to program a model that simulates the dynamics with the Glucocorticoid receptor.

## Goal
  There are two goals for this experiment the fist is to see what for effects changes in the association and dissociation rates couse and changes in regulation couse for the mRNAr. The secons goals is to see if our model is comprebele to a real experiment data. To complete these goals we will create a model that resembles the chemical interactions by the Glucocorticoid receptor. The molecules that change wil then be plotted to show there concentration over time.
  
## hypothese
  I beleve that when the regulation is changed in for the mRNAr that the concentration of every molecule higher stabelizes. And the effects that are coused would be strage.
  

## Theory
These glucocorticoid drugs lower the gene expression of certain proteins that affect the inflammatory responses. When the natural form is made in the body, it reacts in the cytoplasm with the Glucocorticoid receptor, causing it to deform proteins.

Subsequently, the receptor protein binds to the response element, which counteracts the production of certain proteins. The medication works mainly by increasing the production of the proteins. When two enter the cell nucleus, they will dimarisate, causing transcription to form MRNA, which is converted into a protein. These proteins can lead to the breakdown of muscle and fat that it brings back to the liver, after which it is converted back into sugar. This makes the heart work faster and you get more blood to your brain and heart. The inhibition has an effect on the immune system because it reduces the production of cytokines, so that the helper t-cell and the killer-t-cell no longer work well together. In the short term, this should not be a problem, but in the longer term you can get too much glucose and you have an increased chance of becoming sick due to the weakened immune system.

## model
The amount of mRNA that is present at a specific moment (dmRNAr / dt = Ks_Rm - mRna * Kd_Rm) is determined in the cell how many receptors are made (Ks_R) but the receptor is also broken down (Kd_R), so you can say that dR / dt = Ks_R * mRNA - Kd_R * R. This shows that the amount of mRNA production determines how quickly the receptor is produced. The rate at which the medication binds to the receptor is indicated by Kon and the decomposition is indicated by the KT. Only in this case it is the speed of transport to and from the nucleus.\textit{GlucocorticoidReceptorDynamics}


```{r, echo=FALSE, out.width='95%'}
knitr::include_graphics('model_opdracht2.png')
```

## Formulas

$$\frac{\delta mRNA_R}{\delta t} = k_{s_-Rm} * (1-\frac{DR(N)}{IC_{50_-Rm}+DR(N)}) -k_{d_-Rm} * mRNA_R $$  
$$\frac{\delta R}{\delta t} = k_{s_-R} * mRNA_R + R_f * k_{re} * DR(N) - k_{on} * D * R - k_{d_-R} * R$$  
$$\frac{\delta DR}{\delta t} = k_{on} * D * R - k_T * DR$$  
$$\frac{\delta DR(N)}{\delta t} = k_T * DR - k_{re} * DR(N)$$  
Ks_Rm: Stands for the Constant Synthesis of RNA
MRna_R: is a Ration of Rna that also determines the degradation (Kd_Rm) to a large extent
Kd_Rm is the: degradation of RNA which is equal to the production of RNA, otherwise it will not remain constant
IC50_Rm: represents the desired result, namely halving the base value of MPL in the nucleus.

Ks_R is: the production of the free receptor in the glycerol
D: is the concentration of MPL in the plasma
R: is the free receptor
Kd_R: is the breakdown of the receptor in the glycerol

Kon: stands for the binding speed of the MPL complex
DR: the density of the MPL complexes
KT: represents the speed at which it goes to the nucleus.
Rf Kre: represents the returning velocity of the nucleus
DR (N): The concentration of MPL complexes in the nucleus

# Methods

## The software models
We used the programming language R and the library deSolve to create the software model. This is a library with which you can easily implement a model. For this experement we used three models where the first is the basic model where there are no changes made to how it works. The second models is almost identical to the first model where the only change is that the DR(N) complex has no regulatory effect on the receptor mRNA. The last and third model is identical to the first model with a check that changes the D concentration to 0 when the system reaches equalibrium.

### First model
```{r}
# Create a function to simulate the Glucocorticoid receptor dynamica
GRD <- function(t, y, parms){
  with(as.list(c(y, parms)),{
    dmRNAr <- ks_Rm*(1-(DRN/(IC50_rm+DRN)))-kd_Rm*mRNAr
    dR <- ks_r*mRNAr+Rf*kre*DRN-kon*D*R-kd_R*R
    dDR <- kon*D*R-kT*DR
    dDRN <- kT*DR-kre*DRN
    return(list(c(dmRNAr, dR, dDR, dDRN)))
  })
}
```

### Second model
```{r}
# Create a function to simulate the Glucocorticoid receptor dynamica where the DR(N) Complex does not regulate the mRNAr
GRD_no_reg <- function(t, y, parms){
  with(as.list(c(y, parms)),{
    dmRNAr <- ks_Rm-kd_Rm*mRNAr
    dR <- ks_r*mRNAr+Rf*kre*DRN-kon*D*R-kd_R*R
    dDR <- kon*D*R-kT*DR
    dDRN <- kT*DR-kre*DRN
    return(list(c(dmRNAr, dR, dDR, dDRN)))
  })
}
```

### Third model
```{r}
# Create a function to simulate the Glucocorticoid receptor dynamica where when the steady state is reached the concetnraton  of D is set to zero
GRD_second_steady <- function(t, y, parms){
  with(as.list(c(y, parms)),{
    precisie <- 2
    if (steady_state >= 1){
      D <- 0
    }
    dmRNAr <- ks_Rm*(1-(DRN/(IC50_rm+DRN)))-kd_Rm*mRNAr
    dR <- ks_r*mRNAr+Rf*kre*DRN-kon*D*R-kd_R*R
    dDR <- kon*D*R-kT*DR
    dDRN <- kT*DR-kre*DRN
    steady_state <- 0
    #print(c(round(dmRNAr, precisie), round(dR, precisie), round(dDR, precisie),round(dDRN, precisie)))
    if (round(dmRNAr, precisie) == 0 && round(dR, precisie) == 0 && round(dDR, precisie) == 0 && round(dDRN, precisie) == 0){
      steady_state <- 1
    }
    return(list(c(dmRNAr, dR, dDR, dDRN, steady_state)))
  })
}
```

```{r,include=FALSE}
library(deSolve)

# Setting the parameters for the model
parameters <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)

# Set the start values for the simulation
startValues <- c(mRNAr = 4.74, R = 267, DR = 0, DRN = 0)

# Set the time frame. In this case 2 days in hours
times <- seq(0, 48, by=0.1)

# Perform the simulation using the ode method from deSolve
out <- ode(times = times, y = startValues, parms = parameters, func = GRD, method = euler)
```

## Model configuration
\begin{longtable}[l]{l|l|l}
\caption{Parameter Values} \\ \hline
\label{param_table}
$\textbf{Parameter}$             &$\textbf{Value}$& $\textbf{Unit}$              \\ \hline
\endhead
$k_{s-Rm}$       & 2.90  & $fmol/g_{liver}/h$         \\ \hline
$IC_{50_-rm}$       & 26.20  & $fmol/mg_{protein}$         \\ \hline
$k_{on}$       & 0.00329  & $L/nmol/h$         \\ \hline
$k_{T}$       & 0.63  & $1/h$         \\ \hline
$k_{re}$       & 0.57  & $1/h$         \\ \hline
$R_{f}$       & 0.49  & $-$         \\ \hline
$k_{d_-R}$       & 0.0572  & $1/h$         \\ \hline
$k_{d_-Rm}$       & 0.612  & $-$         \\ \hline
$k_{s_-r}$       & 3.22  & $-$         \\ \hline
$D$       & 53.40582  & $nmol/L$         \\ \hline
\end{longtable}

\begin{longtable}[l]{l|l|l}
\caption{Initial State Values} \\ \hline
\label{init_table}
$\textbf{Parameter}$             &$\textbf{Value}$& $\textbf{Unit}$              \\ \hline
\endhead
$mRNA_{r}$       & 4.74  & $fmol/g_{liver}$         \\ \hline
$R$       & 267  & $fmol/mg_{protein}$         \\ \hline
$DR$       & 0  & $fmol/mg_{protein}$         \\ \hline
$DR(N)$       & 0  & $fmol/mg_{protein}$         \\ \hline
\end{longtable}


# Results

## Part 1: What for effects changes in the association and dissociation rates couse and changes in regulation couse for the mRNAr
The graph produced according to the parameters of the model configuration shows that the results match the hypothesis.
```{r echo=FALSE, fig.cap="The system with the standard parameters"}
# Plot the simulated values
plot(out, main=c("Conc. of mRNAr over time", "Conc. of free receptors(R) over time", "Conc. of DR complex over time", "Conc. of nucl. DR complex over time"), xlab = "Time (Hours)", ylab=c("Conc. mRNAr (fmol/g liver)", "Conc. R (fmol/mg protein)", "Conc. DR (fmol/mg protein)", "Conc. DR(N) (fmol/mg protein)"))
```

In first plot of figure 1 you can see that the concetaration of mRNA recepror first dives to 0 before it settles to a constant value of around 2.76 fmol/ g liver. For the second plot of figure 1 you see that the concentration of R starts decreasing quicly until the 30 hour mark where it settels to a constant value of 60.65 fmol/mg protein. In the third and fourth plot of figure 1 you see the concentration of the DR complex in and outside the nucleus, at the start of the simulation you see a large spike in the concentration becouse of the high concentration of mRNA receptor and free reseptors in the system. After these are all used up you can see the concentration levels settle at a value of around 17 fmol/mg protein.

```{r  include=FALSE}
out_no_reg <- ode(times = times, y = startValues, parms = parameters, func = GRD_no_reg, method = euler)
```

```{r  echo=FALSE, fig.height=8, fig.width=8, fig.cap="The system where the DR(N) complex has no effect on mRNAr regulation compaired to de defauld system"}
n <- 4
par(oma = c(2,0,1,0), mfrow = c(floor(sqrt(n)), ceiling(sqrt(n))), mar = c(4, 4, 3, 0))

# A model where the drug has no effect on the syntisise of receptor mRNA
plot(out, out_no_reg, main=c("Conc. of mRNAr over time", "Conc. of free receptors(R) over time", "Conc. of DR complex over time", "Conc. of nucl. DR complex over time"), xlab = "Time (Hours)", ylab=c("Conc. mRNAr (fmol/g liver)", "Conc. R (fmol/mg protein)", "Conc. DR (fmol/mg protein)", "Conc. DR(N) (fmol/mg protein)"), col = c("black", "red"))

par(fig = c(0, 1, 0, 1), oma = c(0, 0, 0, 0), mar = c(0, 0, 0, 0), new = TRUE)

plot(0, 0, type = 'l', bty = 'n', xaxt = 'n', yaxt = 'n')

#legend(-5, 10, col = c("black", "red", "blue", "green", "purple"), c("Default", "k_on/5", "k_on/2", "k_on*5", "k_on*2"), pch = 19)
legend('bottom',legend = c("Default", "No DR(N) regulation"), col = c("black", "red"), lwd = 5, xpd = TRUE, horiz = TRUE, cex = 1, seg.len=1, bty = 'n')

```
To create figure 2 the formula for the mRNAr needed to be changed to the part for the DR(N) regulation is removed.
When the drug has no effect on the synthesis of the reseptor mRNA, you can see that the concentration of mRNAr over time does not change by much. This causes the Concentration of free receptors to drop faster but not further. What all leads to a higher concentration of the DR complex over time both in and outside the nucleus.

```{r,include=FALSE}
# Set the start values for the simulation
startValuesSteady <- c(mRNAr = 4.74, R = 267, DR = 0, DRN = 0, steady_state = 0)

# Set the time frame. In this case 2 days in hours
times_second_steady <- seq(0, 175, by=1)
```

```{r,include=FALSE}
out_second_steady <- ode(times = times_second_steady, y = startValuesSteady, parms = parameters, func = GRD_second_steady, method = euler)
```

```{r  echo=FALSE, fig.height=8, fig.width=8, fig.cap="The system where the drug concentration is set to zero after the system reaches the first equalibrium"}
# A model where the drug is removed when it reaches a steady state
plot(out_second_steady, main=c("Conc. of mRNAr over time", "Conc. of free receptors(R) over time", "Conc. of DR complex over time", "Conc. of nucl. DR complex over time", "Number of Steady states over time"), xlab = "Time (Hours)", ylab=c("Conc. mRNAr (fmol/g liver)", "Conc. R (fmol/mg protein)", "Conc. DR (fmol/mg protein)", "Conc. DR(N) (fmol/mg protein)", "Number of steady state"))
```
In figure 3 you can see that the first 46 hours are the same as de standard model, but after the treatment has stoped the concentration of mRNAr and the concentration of free receptors rockets up close to the starting value at the begining of the simulator. While the Concentrations of the DR complex both in and outside the nucleus drop down to 0 mol/mg protein. This all happens in a time frame of 100 hours with the free receptors as the last thing to balance out.

```{r,include=FALSE}
parameters_k_on_def5 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329/5, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
parameters_k_on_def2 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329/2, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
parameters_k_on_mult5 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329*5, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
parameters_k_on_mult2 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329*2, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)

parameters_k_re_def5 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57/5, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
parameters_k_re_def2 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57/2, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
parameters_k_re_mult5 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57*5, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
parameters_k_re_mult2 <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57*2, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
```

```{r,include=FALSE}
out_k_on_def5 <- ode(times = times, y = startValues, parms = parameters_k_on_def5, func = GRD, method = euler)
out_k_on_def2 <- ode(times = times, y = startValues, parms = parameters_k_on_def2, func = GRD, method = euler)
out_k_on_mult5 <- ode(times = times, y = startValues, parms = parameters_k_on_mult5, func = GRD, method = euler)
out_k_on_mult2 <- ode(times = times, y = startValues, parms = parameters_k_on_mult2, func = GRD, method = euler)

out_k_re_def5 <- ode(times = times, y = startValues, parms = parameters_k_re_def5, func = GRD, method = euler)
out_k_re_def2 <- ode(times = times, y = startValues, parms = parameters_k_re_def2, func = GRD, method = euler)
out_k_re_mult5 <- ode(times = times, y = startValues, parms = parameters_k_re_mult5, func = GRD, method = euler)
out_k_re_mult2 <- ode(times = times, y = startValues, parms = parameters_k_re_mult2, func = GRD, method = euler)
```

```{r  echo=FALSE, fig.height=8, fig.width=8, fig.cap="The effects on the system when the association of the free receptors is changed compaired to the default system"}
n <- 4
par(oma = c(2,0,1,0), mfrow = c(floor(sqrt(n)), ceiling(sqrt(n))), mar = c(4, 4, 3, 0))

plot(out, out_k_on_def5, out_k_on_def2, out_k_on_mult5, out_k_on_mult2, main=c("Conc. of mRNAr over time", "Conc. of free receptors(R) over time", "Conc. of DR complex over time", "Conc. of nucl. DR complex over time"), xlab = "Time (Hours)", ylab=c("Conc. mRNAr (fmol/g liver)", "Conc. R (fmol/mg protein)", "Conc. DR (fmol/mg protein)", "Conc. DR(N) (fmol/mg protein)"), col = c("black", "red", "blue", "green", "purple"))

par(fig = c(0, 1, 0, 1), oma = c(0, 0, 0, 0), mar = c(0, 0, 0, 0), new = TRUE)

plot(0, 0, type = 'l', bty = 'n', xaxt = 'n', yaxt = 'n')

#legend(-5, 10, col = c("black", "red", "blue", "green", "purple"), c("Default", "k_on/5", "k_on/2", "k_on*5", "k_on*2"), pch = 19)
legend('bottom',legend = c("Default", "k_on/5", "k_on/2", "k_on*5", "k_on*2"), col = c("black", "red", "blue", "green", "purple"), lwd = 5, xpd = TRUE, horiz = TRUE, cex = 1, seg.len=1, bty = 'n')

```
In figure 4 you can see with the higher multiplecation of k_on a faster decline in the receptor mRNA and the concentration of free reseptors. With the concentration stabelizing at a lower concentration aswel. At the same time the concentration for DR and DR in the nucleus grows faster and stabalizes at a higher concentration compaired to the base value. While with a higher division of k_on you can see a slower decline in the concentration of receptore mRNA and the concentration of the free receptors, and stabelizing at a higher conentration compaired to the base value. While the concentration of DR and DR in the nucleus grow slower and stabelizing at a lower concentration than the base value. You can also see that the diverence between the values that are devided compaired to the base are greater than the diverence between the values that are multiplicated compaired to the base values.

```{r echo=FALSE, fig.height=8, fig.width=8, fig.cap="The effects on the system when the dissociation of the free receptors is changed compaired to the default system"}

par(oma = c(2,0,1,0), mfrow = c(floor(sqrt(n)), ceiling(sqrt(n))), mar =c(4, 4, 3, 0))

plot(out, out_k_re_def5, out_k_re_def2, out_k_re_mult5, out_k_re_mult2, main=c("Conc. of mRNAr over time", "Conc. of free receptors(R) over time", "Conc. of DR complex over time", "Conc. of nucl. DR complex over time"), xlab = "Time (Hours)", ylab=c("Conc. mRNAr (fmol/g liver)", "Conc. R (fmol/mg protein)", "Conc. DR (fmol/mg protein)", "Conc. DR(N) (fmol/mg protein)"), col = c("black", "red", "blue", "green", "purple"))

par(fig = c(0, 1, 0, 1), oma = c(0, 0, 0, 0), mar = c(0, 0, 0, 0), new = TRUE)

plot(0, 0, type = 'l', bty = 'n', xaxt = 'n', yaxt = 'n')

#legend(-5, 10, col = c("black", "red", "blue", "green", "purple"), c("Default", "k_on/5", "k_on/2", "k_on*5", "k_on*2"), pch = 19)
legend('bottom',legend = c("Default", "k_re/5", "k_re/2", "k_re*5", "k_re*2"), col = c("black", "red", "blue", "green", "purple"), lwd = 5, xpd = TRUE, horiz = TRUE, cex = 1, seg.len=1, bty = 'n')

```
In figure 5 you can see with the multiplecation of k_re that the effects are reversed for the concentration of mRNAr, the concentration of free receptors and the concentration of DR complex in the nucleus. While the higher k_re does not change the peakes at the start for the DR complex you can still see that the concentrain stabalizes at a higher point compaired to the base. This reversal of the effect is also seen in with te values where k_re is devided with the DR complex as a outlier.

The change in k_re has the most impect on the concentration of the mRNAr and the concentration of the DR complex in de nucleus over time. While de change in k_on has the most impect on the concentration of the free receptor and the concetnration of the DR complex over time.

```{r,include=FALSE}
# Setting the parameters for the model
parameters_no_r_syth <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 0, D = 20*1000/374.491)

# Perform the simulation using the ode method from deSolve
out_no_r_syth <- ode(times = times, y = startValues, parms = parameters_no_r_syth, func = GRD, method = euler)
```

```{r echo=FALSE, fig.cap="\\label{fig:figs} The system where the synthesis of the receptor is completly blocked"}
plot(out_no_r_syth, main=c("Conc. of mRNAr over time", "Conc. of free receptors(R) over time", "Conc. of DR complex over time", "Conc. of nucl. DR complex over time"), xlab = "Time (Hours)", ylab=c("Conc. mRNAr (fmol/g liver)", "Conc. R (fmol/mg protein)", "Conc. DR (fmol/mg protein)", "Conc. DR(N) (fmol/mg protein)"))
```
In order to stop the synthisis of of the free receptor in figure 6 the parameter ks_r needs to be zero. This will have the effect that when the starting concentration of free receptor is gone the concentration of DR and DR in the nucleus slowly disappear to zero, with the mRNAr concentration going back to equalibrium as if there was no medicine.

```{r,include=FALSE}
# Setting the parameters for the model
parameters_baseline <- c(ks_Rm = 2.90, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 0.612, ks_r = 3.22, D = 20*1000/374.491)
# Setting the parameters for the model
parameters_ks_rm_def5 <- c(ks_Rm =  2.9/5, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 2.9/5/4.74, ks_r = 3.22, D = 20*1000/374.491)
# Setting the parameters for the model
parameters_ks_rm_def2 <- c(ks_Rm = 2.9/2, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 2.9/2/4.74, ks_r = 3.22, D = 20*1000/374.491)
# Setting the parameters for the model
parameters_ks_rm_mulit5 <- c(ks_Rm = 2.9*2, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 2.9*2/4.74, ks_r = 3.22, D = 20*1000/374.491)
# Setting the parameters for the model
parameters_ks_rm_multi2 <- c(ks_Rm = 2.9*5, IC50_rm = 26.2, kon = 0.00329, kT = 0.63, kre = 0.57, Rf = 0.49, kd_R = 0.0572, kd_Rm = 2.9*5/4.74, ks_r = 3.22, D = 20*1000/374.491)
```

```{r,include=FALSE}
# Perform the simulation using the ode method from deSolve
out_ks_rm_def5 <- ode(times = times, y = startValues, parms = parameters_ks_rm_def5, func = GRD, method = euler)

# Perform the simulation using the ode method from deSolve
out_ks_rm_def2 <- ode(times = times, y = startValues, parms = parameters_ks_rm_def2, func = GRD, method = euler)

# Perform the simulation using the ode method from deSolve
out_ks_rm_mulit5 <- ode(times = times, y = startValues, parms = parameters_ks_rm_mulit5, func = GRD, method = euler)

# Perform the simulation using the ode method from deSolve
out_ks_rm_multi2 <- ode(times = times, y = startValues, parms = parameters_ks_rm_multi2, func = GRD, method = euler)
```

```{r echo=FALSE, fig.height=8, fig.width=8, fig.cap="The effects on the system when the regulation of receptor mRNA is changed"}
n <- 4
par(oma = c(2,0,1,0), mfrow = c(floor(sqrt(n)), ceiling(sqrt(n))), mar = c(4, 4, 3, 0))

plot(out, out_ks_rm_def5, out_ks_rm_def2, out_ks_rm_mulit5, out_ks_rm_multi2, main=c("Conc. of mRNAr over time", "Conc. of free receptors(R) over time", "Conc. of DR complex over time", "Conc. of nucl. DR complex over time"), xlab = "Time (Hours)", ylab=c("Conc. mRNAr (fmol/g liver)", "Conc. R (fmol/mg protein)", "Conc. DR (fmol/mg protein)", "Conc. DR(N) (fmol/mg protein)"), col = c("black", "red", "blue", "green", "purple"))

par(fig = c(0, 1, 0, 1), oma = c(0, 0, 0, 0), mar = c(0, 0, 0, 0), new = TRUE)

plot(0, 0, type = 'l', bty = 'n', xaxt = 'n', yaxt = 'n')

#legend(-5, 10, col = c("black", "red", "blue", "green", "purple"), c("Default", "k_on/5", "k_on/2", "k_on*5", "k_on*2"), pch = 19)
legend('bottom',legend = c("Default", "ks_rm/5", "ks_rm/2", "ks_rm*5", "ks_rm*2"), col = c("black", "red", "blue", "green", "purple"), lwd = 5, xpd = TRUE, horiz = TRUE, cex = 1, seg.len=1, bty = 'n')

```
In figure 7 the effects on the system are minor when the production rate of the receptor mRNA are increased or decreased. The most effect can be observed in the concentration of the mRNAr With the lower production rate taking longer to reach the lowest concentration and with a gratual stabalization. While the increase in the production rate reaches the lowest point faster with a faster rise towards the stabalization point.

## Part 2: Compairing our model to a real experiment data.

The experiment data consists of two seperate measurments one with a drug concentration of 0.1 ng/mL and the second with a drug concentration of 0.3 ng/mL
```{r include=FALSE}
data <- read.csv("MPL.csv", na.strings = "NA")
medians <- aggregate(data[,c("MPL_conc","mRNA","Free_receptor")],list(data$dose,data$time), median, na.rm=T)
names(medians)[1:2] <- c("dose","time")
head(medians)
```

```{r echo=FALSE, fig.cap="Simulated model compairred to experimental data. Plot A and C shows the concentration mRNAr overtime with a drug concentration of 0.1 ng/ml and plot B and D shows the concentration mRNAr overtime with a drug concentration of 0.3 ng/ml"}

# Set the time frame. In this case 2 days in hours
times <- seq(0, 170, by=1)

# Perform the simulation using the ode method from deSolve
out <- ode(times = times, y = startValues, parms = parameters, func = GRD, method = euler) 

par(mfrow=c(2,2))
# Plot the simulated values
plot(out[, c(1,2)], main="A: Conc. of mRNAr over time", xlab = "Time (Hours)", ylab = "Conc. mRNAr (fmol/g liver)", ylim = c(0,5))
lines(mRNA[dose == 0.1]~time[dose == 0.1], data = medians)

plot(out[, c(1,2)], main="B: Conc. of mRNAr over time", xlab = "Time (Hours)", ylab = "Conc. mRNAr (fmol/g liver)", ylim = c(0,5))
lines(mRNA[dose == 0.3]~time[dose == 0.3], data = medians)

plot(out[, c(1,3)], main="C: Conc. of free receptors(R) over time", xlab = "Time (Hours)", ylab = "Conc. R (fmol/mg protein)", ylim = c(0, 300))
lines(Free_receptor[dose == 0.1]~time[dose == 0.1], data = medians)

plot(out[, c(1,3)], main="D: Conc. of free receptors(R) over time", xlab = "Time (Hours)", ylab = "Conc. R (fmol/mg protein)", ylim = c(0, 300))
lines(Free_receptor[dose == 0.3]~time[dose == 0.3], data = medians)

```


In figure 8.A and 8.B you can see that the experimental data does not match the simulated results that we expected the values where far lower than expected. You can also see that de drug concentration in the expriment had no effect on the receptor mRNA concentration.

In figure 8.C and 8.D you can see that the experimental data does relativly mach the simulated results. With the only large change being that the concentration does not stay constant over time after expected equalibrium. You can also see that the drug has effect on the concetration of free receptors, where with a lower drug concetnration the free receptor concentration higher is compaired to the higher drug concentration. 

# Discussion and Conclusion
## Discussion
A weakness of our research is that we look at a narow subsection of a larger proces wich can lead to inperfections in the model used.

## General conclusion and perspective
In our results you can see that the effects caused by even a minor change in association and dissociation rate in can be quite substantial where somtimes the system recieves a masive increase in activity like when k_on was multiplyed bij 5 in figure 4. 
For our second goal we compaired our model to data that was collected, this data did match with our simulated results.

\begin{thebibliography}{9}

\bibitem{Soertaert10}
Soetaert, K., Petzoldt, T., and Woodrow Setzer, R.: \textit{Solving differential equations in R: package deSolve}, J. Stat. Softw., 33, 1-25, 2010.

\bibitem{GlucocorticoidReceptorDynamics}
Ayyar, Vivaswath S. et al. \textit{Mechanistic Multi—Tissue Modeling Of Glucocorticoid-Induced Leucine Zipper Regulation: Integrating Circadian Gene Expression With Receptor-Mediated Corticosteroid Pharmacodynamics}. Journal Of Pharmacology And Experimental Therapeutics, vol 363, no. 1, 2017, pp. 45-57. American Society For Pharmacology & Experimental Therapeutics (ASPET), doi:10.1124/jpet.117.242990. Accessed 25 May 2020.

\end{thebibliography}
